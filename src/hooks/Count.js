import React, { useState, useEffect }  from 'react';
import axios from "axios";

function useStateExemple(){
    const [users, setUsers] = useState([]);

    useEffect(() => {
        axios
            .get("https://jsonplaceholder.typicode.com/users")
            .then(response => setUsers(response.data));
    },[]);

    return (
        <div>
            {console.table(users)}
           <code>{JSON.stringify(users)}</code>
        </div>
    )

}

export default useStateExemple;